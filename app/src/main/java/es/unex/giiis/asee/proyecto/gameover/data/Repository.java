package es.unex.giiis.asee.proyecto.gameover.data;

import androidx.lifecycle.LiveData;

import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.AppExecutors;
import es.unex.giiis.asee.proyecto.gameover.data.Network.GameNetworkDataSource;
import es.unex.giiis.asee.proyecto.gameover.data.Roomdb.VideojuegoDAO;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;


public class Repository {
    private static final String LOG_TAG = Repository.class.getSimpleName();

    // For Singleton instantiation
    private static Repository sInstance;
    private final VideojuegoDAO mDao;
    private final GameNetworkDataSource mVideojuegoNetworkDataSource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();

    private Repository(VideojuegoDAO repoDao, GameNetworkDataSource repoNetworkDataSource) {
        mDao = repoDao;
        mVideojuegoNetworkDataSource = repoNetworkDataSource;
        LiveData<List<Videojuego>> networkData = mVideojuegoNetworkDataSource.getVideojuegosActuales();
        networkData.observeForever(newGamesFromNetwork -> {
            mExecutors.diskIO().execute(() -> {
                if (newGamesFromNetwork.size() > 0){
                    mDao.deleteAll();
                }
                quitarRepetidos(newGamesFromNetwork);
            });
        });
    }

    public synchronized static Repository getInstance(VideojuegoDAO dao, GameNetworkDataSource nds) {
        if (sInstance == null) {
            sInstance = new Repository(dao, nds);
        }
        return sInstance;
    }

    public static Repository getInstance(VideojuegoDAO videojuegoDAO) {
        if (sInstance == null) {
            sInstance = new Repository(sInstance.mDao, sInstance.mVideojuegoNetworkDataSource);
        }
        return sInstance;
    }

    //Correccion code Smell-2 anteriormente no puesto
    public LiveData<List<Videojuego>> getAllVideojuegos() {
        return mDao.getAllVideojuegos();
    }

    public LiveData<List<Videojuego>> getVideojuegosFavoritos() {
        // Todo devuelve el listado de videojuego favoritos
        return mDao.getFavs();
    }

    public LiveData<List<Videojuego>> getVideojuegosCreados() {
        // Todo devuelve el listado de videojuegos creados
        return mDao.getVideojuegosCreados();
    }

    public void addVideojuego(Videojuego videojuego) {
        mExecutors.diskIO().execute(()->{
            mDao.insertVideojuego(videojuego);
        });
    }

    public void updateVideojuego(Videojuego videojuego) {
        mExecutors.diskIO().execute(()->{
            mDao.updateVideojuego(videojuego);
        });
    }

    public LiveData<Videojuego> getVideojuego(Videojuego videojuego) {
       return mDao.getVideojuego(videojuego.getId());
    }

    public void deleteVideojuego(Videojuego videojuego){
        mExecutors.diskIO().execute(()->{
            mDao.deleteVideojuego(videojuego);
        });
    }

    //Procedimiento para buscar videojuego en la BD
    Videojuego buscarVideojuego(Videojuego v, List<Videojuego> VBD){
        boolean enc=false;
        int i=0; //indice
        Videojuego vBd = null;
        while(i<VBD.size() && enc==false){
            vBd = VBD.get(i);
            if(v.getTitle().equals(vBd.getTitle())==true){
                enc = true;
            }
            i++;
        }
        //Para evitar que se haga una ultima insercion
        if(enc == false){
            vBd = null;
        }
        return vBd;
    }

    //Evitamos repetidas, a la hora de insercion de recetas puntuadas
    public void quitarRepetidos(List<Videojuego> VAPI) {
        Videojuego vBd;//Aux
        List<Videojuego> RBD;
        RBD = mDao.getAllVideojuegos().getValue();

        if (RBD != null) {
            for (Videojuego vApi : VAPI) {
                //Buscamos la receta de la API en la base de datos, por si actualizada
                vBd = buscarVideojuego(vApi, RBD);
                if (vBd != null) {//Si no existe en la BD, inserto la de la API
                    //Actualizacion
                    vApi.setEsFavorito(vBd.isEsFavorito());
                    vApi.setMiPuntuacion(vBd.getMiPuntuacion());
                    vApi.setComentario(vBd.getComentario());
                }
                mDao.updateVideojuego(vApi);
                vBd = null;
            }
        } else {
            //La primera carga si la bd esta vacia
            for (Videojuego vApi : VAPI) {
                mDao.insertVideojuego(vApi);
            }
        }
    }

} 