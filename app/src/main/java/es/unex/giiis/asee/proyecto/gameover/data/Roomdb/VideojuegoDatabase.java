package es.unex.giiis.asee.proyecto.gameover.data.Roomdb;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

@Database(entities = {Videojuego.class}, version = 1,exportSchema = false)
public abstract class VideojuegoDatabase extends RoomDatabase {

    private static VideojuegoDatabase instance;

    public static VideojuegoDatabase getDatabase(Context context){
        if(instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(), VideojuegoDatabase.class, "videojuego.db").build();
        return instance;
    }
    public abstract VideojuegoDAO videojuegoDAO();
}

