package es.unex.giiis.asee.proyecto.gameover.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.AppContainer;
import es.unex.giiis.asee.proyecto.gameover.MyApplication;
import es.unex.giiis.asee.proyecto.gameover.R;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;
import es.unex.giiis.asee.proyecto.gameover.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment implements GamesAdapter.OnListInteractionListener, SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener{
    private FragmentHomeBinding binding;


    private OnFragmentInteractionListener mListener;
    private GamesAdapter gAdapter;
    RecyclerView recyclerJuegos;
    private LinearLayoutManager LayoutManager;
    private HomeViewModel mViewModel;

    public HomeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View vista=inflater.inflate(R.layout.fragment_home,container,false);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.homeFactory).get(HomeViewModel.class);

        recyclerJuegos = (RecyclerView) vista.findViewById(R.id.recyclerId);
        LayoutManager = new LinearLayoutManager(getActivity());
        recyclerJuegos.setLayoutManager(LayoutManager);

        List<Videojuego> listaVideojuegos = new ArrayList<Videojuego>();
        gAdapter = new GamesAdapter(listaVideojuegos,this);
        mViewModel.getVideojuegos().observe(getViewLifecycleOwner(), videojuegos -> {
            gAdapter.clear();
            gAdapter.swap(videojuegos);
        });

        gAdapter.setItemClickListener(new GamesAdapter.ItemClickListener() {
            public void onItemClick(Videojuego videojuego) {
                Intent intent = new Intent(getContext(), MostrarJuegoActivity.class);
                intent.putExtra("Videojuego", (Serializable) videojuego);
                startActivity(intent);
            }

        });
        recyclerJuegos.setAdapter(gAdapter);

        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onListInteraction(String url) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Search");

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (s == null || s.trim().isEmpty()) {
            resetSearch();
            return false;
        }

        ArrayList<Videojuego> filteredValues = new ArrayList<Videojuego>();
        ArrayList<Videojuego> aux = new ArrayList<Videojuego>();
        mViewModel.getVideojuegos().observe(getViewLifecycleOwner(), videojuegos -> {
            //Para mantenerlas tos en el filtrado
            aux.clear();
            aux.addAll(videojuegos);
        });
        for (Videojuego value : aux) {
            if (value.getTitle().toLowerCase().contains(s.toLowerCase())) {
                filteredValues.add(value);
            }
        }

        gAdapter.swap(filteredValues);

        return false;
    }

    private void resetSearch() {
        ArrayList<Videojuego> aux = new ArrayList<Videojuego>();
        mViewModel.getVideojuegos().observe(getViewLifecycleOwner(), videojuegos -> {
            //Para mantenerlas tos en el filtrado
            aux.clear();
            aux.addAll(videojuegos);
        });
        gAdapter.swap(aux);
    }


    @Override
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        return true;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}