package es.unex.giiis.asee.proyecto.gameover.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.AppContainer;
import es.unex.giiis.asee.proyecto.gameover.MyApplication;
import es.unex.giiis.asee.proyecto.gameover.R;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;
import es.unex.giiis.asee.proyecto.gameover.databinding.FragmentGalleryBinding;

public class FavoritosFragment extends Fragment implements GamesAdapter.OnListInteractionListener{
    private FragmentGalleryBinding binding;


    private OnFragmentInteractionListener mListener;
    private GamesAdapter gAdapter;
    RecyclerView recyclerJuegos;
    private LinearLayoutManager LayoutManager;
    private FavoritosViewModel mViewModel;
    private List<Videojuego> videojuegosFavoritos;

    public FavoritosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListaPersonajesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoritosFragment newInstance(String param1, String param2) {
        FavoritosFragment fragment= new FavoritosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View vista=inflater.inflate(R.layout.fragment_gallery,container,false);
        recyclerJuegos= (RecyclerView) vista.findViewById(R.id.recyclerId);
        LayoutManager = new LinearLayoutManager(getActivity());
        recyclerJuegos.setLayoutManager(LayoutManager);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.favoritosFactory).get(FavoritosViewModel.class);

        videojuegosFavoritos = new ArrayList<Videojuego>();
        gAdapter = new GamesAdapter(videojuegosFavoritos,this);
        mViewModel.getVideojuegosFavoritos().observe(getViewLifecycleOwner(), videojuegos -> {
            gAdapter.clear();
            gAdapter.swap(videojuegos);
        });


        gAdapter.setItemClickListener(new GamesAdapter.ItemClickListener() {
            public void onItemClick(Videojuego videojuego) {
                Intent intent = new Intent(getContext(), MostrarJuegoActivity.class);
                intent.putExtra("Videojuego", (Serializable) videojuego);
                startActivity(intent);
            }

        });
        recyclerJuegos.setAdapter(gAdapter);

        return vista;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onListInteraction(String url) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}