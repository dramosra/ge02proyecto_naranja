package es.unex.giiis.asee.proyecto.gameover;

import android.content.Context;

import es.unex.giiis.asee.proyecto.gameover.data.Network.GameNetworkDataSource;
import es.unex.giiis.asee.proyecto.gameover.data.Repository;
import es.unex.giiis.asee.proyecto.gameover.data.Roomdb.VideojuegoDatabase;
import es.unex.giiis.asee.proyecto.gameover.ui.CreadosViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.CrearJuegoViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.EditarVideojuegoViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.FavoritosViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.HomeViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.MostrarJuegoViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.TopVendidosViewModelFactory;

public class AppContainer {

    private VideojuegoDatabase database;
    private GameNetworkDataSource networkDataSource;
    public Repository repository;
    public HomeViewModelFactory homeFactory;
    public EditarVideojuegoViewModelFactory editarJuegoFactory;
    public CrearJuegoViewModelFactory crearJuegoFactory;
    public MostrarJuegoViewModelFactory mostrarJuegoFactory;
    public FavoritosViewModelFactory favoritosFactory;
    public TopVendidosViewModelFactory topVendidosFactory;
    public CreadosViewModelFactory creadosFactory;


    public AppContainer(Context context){
        database = VideojuegoDatabase.getDatabase(context);
        networkDataSource = GameNetworkDataSource.getInstance();
        repository = Repository.getInstance(database.videojuegoDAO(), networkDataSource);
        homeFactory = new HomeViewModelFactory(repository);
        editarJuegoFactory = new EditarVideojuegoViewModelFactory(repository);
        crearJuegoFactory = new CrearJuegoViewModelFactory(repository);
        mostrarJuegoFactory = new MostrarJuegoViewModelFactory(repository);
        favoritosFactory = new FavoritosViewModelFactory(repository);
        topVendidosFactory = new TopVendidosViewModelFactory(repository);
        creadosFactory = new CreadosViewModelFactory(repository);
    }
}
