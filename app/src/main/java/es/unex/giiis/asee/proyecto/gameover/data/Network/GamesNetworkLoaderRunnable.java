package es.unex.giiis.asee.proyecto.gameover.data.Network;

import java.io.IOException;
import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.AppExecutors;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;
import es.unex.giiis.asee.proyecto.gameover.ui.OnGamesLoadedListener;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class GamesNetworkLoaderRunnable implements Runnable {
    private final OnGamesLoadedListener mOnGamesLoadedListener;


    public GamesNetworkLoaderRunnable(OnGamesLoadedListener onGamesLoadedListener){
        mOnGamesLoadedListener = onGamesLoadedListener;
    }

    public interface gamesInterface {
        @GET("api/1.0/deals")
        Call<List<Videojuego>> listGames();
    }

    //Evitamos repetidos, a la hora de insercion de videojuegos puntuados

    @Override
    public void run() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.cheapshark.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        gamesInterface interfaceGames = retrofit.create(gamesInterface.class);
        try {
            List<Videojuego> games = interfaceGames.listGames().execute().body();

            AppExecutors.getInstance().mainThread().execute(()->mOnGamesLoadedListener.onGamesLoaded(games));
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

}
