package es.unex.giiis.asee.proyecto.gameover.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.AppContainer;
import es.unex.giiis.asee.proyecto.gameover.MyApplication;
import es.unex.giiis.asee.proyecto.gameover.R;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

public class CreadosFragment extends Fragment implements GamesAdapter.OnListInteractionListener{

    private OnFragmentInteractionListener mListener;
    private GamesAdapter gAdapter;
    RecyclerView recyclerJuegos;
    private LinearLayoutManager LayoutManager;
    private FloatingActionButton fab;
    private CreadosViewModel mViewModel;
    private List<Videojuego> videojuegosCreados;

    public CreadosFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CreadosFragment newInstance(String param1, String param2) {
        CreadosFragment fragment= new CreadosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View vista=inflater.inflate(R.layout.fragment_mis_creados,container,false);
        recyclerJuegos= (RecyclerView) vista.findViewById(R.id.recyclerId);
        LayoutManager = new LinearLayoutManager(getActivity());
        recyclerJuegos.setLayoutManager(LayoutManager);
        fab = (FloatingActionButton) vista.findViewById(R.id.fab);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.creadosFactory).get(CreadosViewModel.class);



        videojuegosCreados = new ArrayList<Videojuego>();
        gAdapter = new GamesAdapter(videojuegosCreados,this);
        mViewModel.getVideojuegosCreados().observe(getViewLifecycleOwner(), videojuegos -> {
            gAdapter.clear();
            gAdapter.swap(videojuegos);
        });

        gAdapter.setItemClickListener(new GamesAdapter.ItemClickListener() {
            public void onItemClick(Videojuego videojuego) {
                Intent intent = new Intent(getContext(), MostrarJuegoActivity.class);
                intent.putExtra("Videojuego", (Serializable) videojuego);
                startActivity(intent);
            }

        });
        recyclerJuegos.setAdapter(gAdapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CrearJuegoActivity.class);
                startActivity(intent);
            }
        });

        return vista;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onListInteraction(String url) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}