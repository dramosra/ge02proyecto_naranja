package es.unex.giiis.asee.proyecto.gameover.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.data.Repository;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

/**
 * {@link ViewModel} for {@link MainActivity}
 */
class FavoritosViewModel extends ViewModel {

    private final Repository mRepository;
    private final LiveData<List<Videojuego>> mRepos;

    public FavoritosViewModel(Repository repository) {
        mRepository = repository;
        mRepos = mRepository.getVideojuegosFavoritos();
    }



    public LiveData<List<Videojuego>> getVideojuegosFavoritos() {
        return mRepos;
    }


}
