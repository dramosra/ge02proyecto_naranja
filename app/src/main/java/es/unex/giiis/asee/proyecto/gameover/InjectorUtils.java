package es.unex.giiis.asee.proyecto.gameover;

import android.content.Context;

import es.unex.giiis.asee.proyecto.gameover.data.Network.GameNetworkDataSource;
import es.unex.giiis.asee.proyecto.gameover.data.Repository;
import es.unex.giiis.asee.proyecto.gameover.data.Roomdb.VideojuegoDatabase;
import es.unex.giiis.asee.proyecto.gameover.ui.CreadosViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.FavoritosViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.HomeViewModelFactory;
import es.unex.giiis.asee.proyecto.gameover.ui.TopVendidosViewModelFactory;

public class InjectorUtils {

    public static Repository provideRepository(Context context) {
        VideojuegoDatabase database = VideojuegoDatabase.getDatabase(context.getApplicationContext());
        GameNetworkDataSource networkDataSource = GameNetworkDataSource.getInstance();
        return Repository.getInstance(database.videojuegoDAO(),networkDataSource);
    }

    public static HomeViewModelFactory provideHomeViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new HomeViewModelFactory(repository);
    }

    public static FavoritosViewModelFactory provideFavoritosViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new FavoritosViewModelFactory(repository);
    }

    public static TopVendidosViewModelFactory provideTopVendidosActivityViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new TopVendidosViewModelFactory(repository);
    }

    public static CreadosViewModelFactory provideCreadosViewModelFactory(Context context) {
        Repository repository = provideRepository(context.getApplicationContext());
        return new CreadosViewModelFactory(repository);
    }

}