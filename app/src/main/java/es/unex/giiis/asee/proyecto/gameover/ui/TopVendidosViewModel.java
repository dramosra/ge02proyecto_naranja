package es.unex.giiis.asee.proyecto.gameover.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.data.Repository;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

/**
 * {@link ViewModel} for {@link MainActivity}
 */
class TopVendidosViewModel extends ViewModel {

    private final Repository mRepository;
    private final LiveData<List<Videojuego>> mRepos;

    public TopVendidosViewModel(Repository repository) {
        mRepository = repository;
        mRepos = mRepository.getAllVideojuegos();
    }

    public LiveData<List<Videojuego>> getAllVideojuegos() {
        return mRepos;
    }


}
