
package es.unex.giiis.asee.proyecto.gameover.data.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "videojuegos")
public class Videojuego implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Integer id;
    @SerializedName("internalName")
    @Expose
    @ColumnInfo(name = "titulo")
    private String internalName;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("metacriticLink")
    @Expose
    @Ignore
    private String metacriticLink;
    @SerializedName("dealID")
    @Expose
    @Ignore
    private String dealID;
    @SerializedName("storeID")
    @Expose
    @ColumnInfo(name = "storeID")
    private String storeID;
    @SerializedName("gameID")
    @Expose
    @Ignore
    private String gameID;
    @SerializedName("salePrice")
    @Expose
    @NonNull
    @ColumnInfo(name = "precioVenta")
    private String salePrice;
    @SerializedName("normalPrice")
    @Expose
    @NonNull
    @ColumnInfo(name = "precioNormal")
    private String normalPrice;
    @SerializedName("isOnSale")
    @Expose
    @ColumnInfo(name = "enVenta")
    private String isOnSale;
    @SerializedName("savings")
    @Expose
    @ColumnInfo(name = "numeroVendidos")
    private String savings;
    @SerializedName("metacriticScore")
    @Expose
    @ColumnInfo(name = "criticaMetaCritic")
    private String metacriticScore;
    @SerializedName("steamRatingText")
    @Expose
    @ColumnInfo(name = "nivelCritica")
    private String steamRatingText;
    @SerializedName("steamRatingPercent")
    @Expose
    @ColumnInfo(name = "criticaPorcentaje")
    private String steamRatingPercent;
    @SerializedName("steamRatingCount")
    @Expose
    @ColumnInfo(name = "numeroPuntuaciones")
    private String steamRatingCount;
    @SerializedName("steamAppID")
    @Expose
    @Ignore
    private String steamAppID;
    @SerializedName("releaseDate")
    @Expose
    @Ignore
    private Integer releaseDate;
    @SerializedName("lastChange")
    @Expose
    @Ignore
    private Integer lastChange;
    @SerializedName("dealRating")
    @Expose
    @ColumnInfo(name = "valoracion")
    private String dealRating;
    @SerializedName("thumb")
    @Expose
    @ColumnInfo(name = "Imagen")
    private String thumb;

    @ColumnInfo(name = "usuarioVideojuego")
    private boolean esUsuario;

    @ColumnInfo(name = "videojuegoFavorito")
    private boolean esFavorito;

    @ColumnInfo(name = "comentario")
    private  String comentario;

    @ColumnInfo(name = "estadoVenta")
    private  String estadoVenta;

    @ColumnInfo(name = "miPuntuación")
    private  int miPuntuacion;

    public int getMiPuntuacion() {
        return miPuntuacion;
    }

    public void setMiPuntuacion(int miPuntuacion) {
        this.miPuntuacion = miPuntuacion;
    }

    public String getEstadoVenta(){
        return this.estadoVenta;
    }

    public void setEstadoVenta(String estadoVenta) {
        this.estadoVenta = estadoVenta;
    }

    public String getComentario(){
        return this.comentario;
    }

    public void setComentario(String comentario){
        this.comentario = comentario;
    }

    public boolean isEsUsuario() {
        return esUsuario;
    }

    public void setEsUsuario(boolean esUsuario) {
        this.esUsuario = esUsuario;
    }

    public boolean isEsFavorito() {
        return esFavorito;
    }

    public void setEsFavorito(boolean esFavorito) {
        this.esFavorito = esFavorito;
    }

    public String getInternalName() {
        return internalName;
    }

    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMetacriticLink() {
        return metacriticLink;
    }

    public void setMetacriticLink(String metacriticLink) {
        this.metacriticLink = metacriticLink;
    }

    public String getDealID() {
        return dealID;
    }

    public void setDealID(String dealID) {
        this.dealID = dealID;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getGameID() {
        return gameID;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(String normalPrice) {
        this.normalPrice = normalPrice;
    }

    public String getIsOnSale() {
        return isOnSale;
    }

    public void setIsOnSale(String isOnSale) {
        this.isOnSale = isOnSale;
    }

    public String getSavings() {
        return savings;
    }

    public void setSavings(String savings) {
        this.savings = savings;
    }

    public String getMetacriticScore() {
        return metacriticScore;
    }

    public void setMetacriticScore(String metacriticScore) {
        this.metacriticScore = metacriticScore;
    }

    public String getSteamRatingText() {
        return steamRatingText;
    }

    public void setSteamRatingText(String steamRatingText) {
        this.steamRatingText = steamRatingText;
    }

    public String getSteamRatingPercent() {
        return steamRatingPercent;
    }

    public void setSteamRatingPercent(String steamRatingPercent) {
        this.steamRatingPercent = steamRatingPercent;
    }

    public String getSteamRatingCount() {
        return steamRatingCount;
    }

    public void setSteamRatingCount(String steamRatingCount) {
        this.steamRatingCount = steamRatingCount;
    }

    public String getSteamAppID() {
        return steamAppID;
    }

    public void setSteamAppID(String steamAppID) {
        this.steamAppID = steamAppID;
    }

    public Integer getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Integer releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getLastChange() {
        return lastChange;
    }

    public void setLastChange(Integer lastChange) {
        this.lastChange = lastChange;
    }

    public String getDealRating() {
        return dealRating;
    }

    public void setDealRating(String dealRating) {
        this.dealRating = dealRating;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    //Correccion Bug null pointer Sonar_3
    @Override
    public boolean equals(@Nullable Object obj) {
        try {
            if(obj!=null) {
                return this.id == ((Videojuego) obj).getId();
            }
            else{
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }


}
