package es.unex.giiis.asee.proyecto.gameover.ui;

import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

public interface OnGamesLoadedListener {
    public void onGamesLoaded(List<Videojuego> games);
}
