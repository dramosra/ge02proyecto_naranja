package es.unex.giiis.asee.proyecto.gameover.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.squareup.picasso.Picasso;

import es.unex.giiis.asee.proyecto.gameover.AppContainer;
import es.unex.giiis.asee.proyecto.gameover.AppExecutors;
import es.unex.giiis.asee.proyecto.gameover.MyApplication;
import es.unex.giiis.asee.proyecto.gameover.R;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

public class EditarVideojuegoActivity extends AppCompatActivity {

    private EditText titulo;
    private EditText valoracion;
    private EditText precio_Salida;
    private EditText precio_Venta;
    private EditText num_Ventas;
    private Button EFbutton;
    private ImageView Imagen;
    private AppExecutors mExecutors;
    Videojuego videojuego;
    private EditarVideojuegoViewModel mViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setContentView(R.layout.activity_edit_game);

        titulo = (EditText) findViewById(R.id.valorTitulo);
        valoracion = (EditText) findViewById(R.id.valorValoracion);
        precio_Salida = (EditText) findViewById(R.id.valorSalida);
        precio_Venta = (EditText) findViewById(R.id.valorRebajado);
        num_Ventas = (EditText) findViewById(R.id.valorVendidos);
        Imagen = (ImageView) findViewById(R.id.id_Imagen);
        EFbutton = (Button) findViewById(R.id.button);
        mExecutors = AppExecutors.getInstance();

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.editarJuegoFactory).get(EditarVideojuegoViewModel.class);

        videojuego = (Videojuego) getIntent().getSerializableExtra("Videojuego");

        Log.i("Editar",videojuego.getTitle());

        if(videojuego.getTitle()==null){
            titulo.setText("Titulo");
        }else{
            titulo.setText(videojuego.getTitle());
        }

        if(videojuego.getDealRating()==null){
            valoracion.setText("Valoracion");
        }else{
            valoracion.setText(videojuego.getDealRating());
        }

        if(videojuego.getNormalPrice()==null){
            precio_Salida.setText("Precio Salida");

        }else{
            precio_Salida.setText(videojuego.getNormalPrice());
            Log.i("VIDEOJUEGO",videojuego.getNormalPrice());
        }

        if(videojuego.getSalePrice()==null){
            precio_Venta.setText("Precio Venta");
        }else{
            precio_Venta.setText(videojuego.getSalePrice());
        }

        if(videojuego.getSavings()==null){
            num_Ventas.setText("Numero de Ventas");
        }else{
            num_Ventas.setText(videojuego.getSavings());
        }

        if(videojuego.getThumb() != null){
            Picasso.get().load(videojuego.getThumb()).into(Imagen);
        }else{
            Picasso.get().load("https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png").into(Imagen);
        }

        EFbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titulo.setError(null);
                valoracion.setError(null);
                precio_Salida.setError(null);
                precio_Venta.setError(null);
                num_Ventas.setError(null);

                if("".equals(titulo.getText().toString())){
                    titulo.setError("Introduce un titulo");
                    titulo.requestFocus();
                    return;
                }else{
                    Log.i("Titulo",titulo.getText().toString());
                    videojuego.setTitle(titulo.getText().toString());
                }

                if("".equals(valoracion.getText().toString())){
                    valoracion.setError("Introduce la valoracion");
                    valoracion.requestFocus();
                    return;
                }else if(Integer.parseInt(valoracion.getText().toString())>=0&&Integer.parseInt(valoracion.getText().toString())<=10){
                    Log.i("Valoracion",valoracion.getText().toString());
                    videojuego.setDealRating(valoracion.getText().toString());
                }else {
                    valoracion.setError("La valoracion debe estar comprendida entre 0 y 10");
                    valoracion.requestFocus();
                    return;
                }

                if("".equals(precio_Salida.getText().toString())){
                    precio_Salida.setError("Introduce un precio de Salida");
                    precio_Salida.requestFocus();
                    return;
                }else if(precio_Venta.getText().toString().isEmpty()){
                    Log.i("precio_Salida",precio_Salida.getText().toString());
                    videojuego.setNormalPrice(precio_Salida.getText().toString());
                }else if(Integer.parseInt(precio_Salida.getText().toString())<Integer.parseInt(precio_Venta.getText().toString())){
                    precio_Salida.setError("El precio de salida es menor que el de rebaja");
                    precio_Salida.requestFocus();
                    return;
                }else{
                    Log.i("precio_Venta",precio_Salida.getText().toString());
                    videojuego.setNormalPrice(precio_Salida.getText().toString());
                }

                if("".equals(precio_Venta.getText().toString())){
                    precio_Venta.setError("Introduce un precio de Venta");
                    precio_Venta.requestFocus();
                    return;
                }else if(precio_Salida.getText().toString().isEmpty()) {
                    Log.i("precio_Venta",precio_Venta.getText().toString());
                    videojuego.setSalePrice(precio_Venta.getText().toString());
                }else if(Integer.parseInt(precio_Salida.getText().toString())<Integer.parseInt(precio_Venta.getText().toString())){
                    precio_Venta.setError("El precio de rebaja es mucho mayor que el de salida");
                    precio_Venta.requestFocus();
                    return;
                }else{
                    Log.i("precio_Venta",precio_Venta.getText().toString());
                    videojuego.setSalePrice(precio_Venta.getText().toString());
                }

                if("".equals(num_Ventas.getText().toString())){
                    num_Ventas.setError("Introduce un numero de ventas");
                    num_Ventas.requestFocus();
                    return;
                }else{
                    Log.i("num_Ventas",num_Ventas.getText().toString());
                    videojuego.setSavings(num_Ventas.getText().toString());
                }
                if (titulo.getError()==null&&valoracion.getError()==null&&precio_Salida.getError()==null&&precio_Venta.getError()==null&&num_Ventas.getError()==null) {
                    mViewModel.actualizarVideojuego(videojuego);
                    Toast.makeText(getApplicationContext(), "Videojuego actualizado", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Videojuego actualizado", Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }
}
