package es.unex.giiis.asee.proyecto.gameover.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.proyecto.gameover.data.Repository;

public class TopVendidosViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final Repository mRepository;

    public TopVendidosViewModelFactory(Repository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new TopVendidosViewModel(mRepository);
    }
}