package es.unex.giiis.asee.proyecto.gameover.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

import es.unex.giiis.asee.proyecto.gameover.AppContainer;
import es.unex.giiis.asee.proyecto.gameover.MyApplication;
import es.unex.giiis.asee.proyecto.gameover.R;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;


public class MostrarJuegoActivity extends AppCompatActivity {

    private TextView titulo;
    private TextView valoracion;
    private TextView precio_Salida;
    private TextView precio_Venta;
    private TextView num_Ventas;
    private TextView comentario;
    private TextView textComentario;
    private TextView textEstadoVenta;
    private TextView estadoVenta;
    private ImageView Imagen;
    private TextView puntuacionvalue;
    FloatingActionButton EFbutton;
    private MostrarJuegoViewModel mViewModel;
    private Videojuego videojuego1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setContentView(R.layout.activity_mostrar_juego);

        titulo = (TextView) findViewById(R.id.valorTitulo);
        valoracion = (TextView) findViewById(R.id.valorValoracion);
        precio_Salida = (TextView) findViewById(R.id.valorSalida);
        precio_Venta = (TextView) findViewById(R.id.valorRebajado);
        num_Ventas = (TextView) findViewById(R.id.valorVendidos);
        comentario = (TextView) findViewById(R.id.coment);
        textComentario = (TextView) findViewById(R.id.textComent);
        textEstadoVenta = (TextView) findViewById(R.id.textEstadoVenta);
        estadoVenta = (TextView) findViewById(R.id.estadoVenta);
        EFbutton = (FloatingActionButton) findViewById(R.id.fav_Botton);
        Imagen = (ImageView) findViewById(R.id.id_Imagen);
        puntuacionvalue = (TextView)findViewById(R.id.puntuacionValue);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.mostrarJuegoFactory).get(MostrarJuegoViewModel.class);

        videojuego1 = (Videojuego) getIntent().getSerializableExtra("Videojuego");

        if(videojuego1.getTitle()==null){
            titulo.setText("Titulo");
        }else{
            titulo.setText(videojuego1.getTitle());
        }

        if(videojuego1.getDealRating()==null){
            valoracion.setText("Valoracion");
        }else{
            valoracion.setText(videojuego1.getDealRating());
        }

        if(videojuego1.getNormalPrice()==null){
            precio_Salida.setText("Precio Salida");

        }else{
            precio_Salida.setText(videojuego1.getNormalPrice());
            Log.i("VIDEOJUEGO",videojuego1.getNormalPrice());
        }

        if(videojuego1.getSalePrice()==null){
            precio_Venta.setText("Precio Venta");
        }else{
            precio_Venta.setText(videojuego1.getSalePrice());
        }

        if(videojuego1.getSavings()==null){
            num_Ventas.setText("Numero de Ventas");
        }else{
            int n = Integer.parseInt(videojuego1.getSavings().substring(0,videojuego1.getSavings().indexOf('.')));
            num_Ventas.setText(Integer.toString(n) + " millones");
        }

        if(videojuego1.getEstadoVenta()==null){
            estadoVenta.setText("SI");
        }else{
            Log.i("VIDEOJUEGO",videojuego1.getEstadoVenta());
            estadoVenta.setText(videojuego1.getEstadoVenta());
        }

        if(videojuego1.getComentario()==null){
            comentario.setVisibility(View.GONE);
            textComentario.setVisibility(View.GONE);
        }else{
            comentario.setVisibility(View.VISIBLE);
            textComentario.setVisibility(View.VISIBLE);
            comentario.setText(videojuego1.getComentario());
            Log.i("VIDEOJUEGO",videojuego1.getComentario());
        }

        if(videojuego1.isEsFavorito() == true){
            EFbutton.setImageResource(R.drawable.ic_baseline_star_rate_24);
        }
        if(videojuego1.getThumb() != null){
            Picasso.get().load(videojuego1.getThumb()).into(Imagen);
        }else{
            Picasso.get().load("https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png").into(Imagen);
        }
        if(videojuego1.getMiPuntuacion()<0 || videojuego1.getMiPuntuacion()>10){
            videojuego1.setMiPuntuacion(0);
            mViewModel.actualizarVideojuego(videojuego1);
        }else{
            puntuacionvalue.setText(Integer.toString(videojuego1.getMiPuntuacion()));
        }
        Log.i("VIDEOJUEGO",Integer.toString(videojuego1.getMiPuntuacion()));

    }
    //Tener en cuenta de que si falla el menu puede ser por invalidate options menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if(videojuego1.isEsUsuario()) {
            if(videojuego1.getComentario()==null){
                Log.i("COMENTARIO","SIN COMENTARIO");
                getMenuInflater().inflate(R.menu.main, menu);
                menu.setGroupVisible(0, true);
            }else{
                Log.i("COMENTARIO","CON COMENTARIO");
                getMenuInflater().inflate(R.menu.menu_delete_com, menu);
                menu.setGroupVisible(0, true);
            }
        }else {
            if(videojuego1.getComentario()==null){
                getMenuInflater().inflate(R.menu.menu_no_creados, menu);
                menu.setGroupVisible(0, true);
            }else{
                getMenuInflater().inflate(R.menu.menu_delete_com2, menu);
                menu.setGroupVisible(0, true);
            }

        }
        return true;
    }

    /*
    CONTROLA EL BOTÓN QUE PULSAMOS EN EL MENÚ
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.addComent:
                addComent();
                break;
            case R.id.addEstadoVenta:
                addEstadoVenta();
                break;
            case R.id.addPrecioSalida:
                addPrecioSalida();
                break;
            case R.id.addPuntuacion:
                addPuntuacion();
                break;
            case R.id.delete:
                deleteVideojuego();
                break;
            case R.id.editGame:
                editGame();
                break;
            case R.id.deleteComent:
                deleteComment();
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }



    /*
    MÉTODOS PARA AÑADIR CAMPOS A UN VIDEOJUEGO YA CREADO, A PARTIR DEL MENÚ
     */
    private void deleteComment() {
        videojuego1.setComentario(null);
        mViewModel.actualizarVideojuego(videojuego1);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        Toast.makeText(getApplicationContext(), "Comentario eliminado con éxito", Toast.LENGTH_LONG).show();
    }

    private void editGame() {
        Intent intent = new Intent(this, EditarVideojuegoActivity.class);
        intent.putExtra("Videojuego", (Serializable) videojuego1);
        startActivity(intent);
    }
    public void addComent(){
        EditText comentario;
        comentario = new EditText(MostrarJuegoActivity.this);
        AlertDialog.Builder alertaP = new AlertDialog.Builder(MostrarJuegoActivity.this);
        alertaP.setView(comentario)
                .setMessage("Comentar")
                .setCancelable(false)
                .setPositiveButton("Comentar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        videojuego1.setComentario(comentario.getText().toString());
                        mViewModel.actualizarVideojuego(videojuego1);
                        Toast.makeText(getApplicationContext(), "Comentario añadido", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog tituloP = alertaP.create();
        tituloP.setTitle("Añadir comentario al videojuego");
        tituloP.show();
    }
    public void addEstadoVenta(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MostrarJuegoActivity.this);
        alertDialog.setTitle("Añadir estado venta al videojuego");
        String[] items = {"SI","NO"};
        int checkedItem = 1;
        alertDialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        videojuego1.setEstadoVenta(items[0]);
                        break;
                    case 1:
                        videojuego1.setEstadoVenta(items[1]);
                        break;
                    default:
                        //Code Smell_1
                        videojuego1.setEstadoVenta("S/N");
                }
                mViewModel.actualizarVideojuego(videojuego1);
                Toast.makeText(MostrarJuegoActivity.this, "Estado de venta actualizado", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }
    public void addPrecioSalida(){
        EditText precioSalida;
        precioSalida = new EditText(MostrarJuegoActivity.this);
        AlertDialog.Builder alertaP = new AlertDialog.Builder(MostrarJuegoActivity.this);
        alertaP.setView(precioSalida)
                .setMessage("Modificar")
                .setCancelable(false)
                .setPositiveButton("Añadir Precio", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        videojuego1.setNormalPrice(precioSalida.getText().toString());
                        mViewModel.actualizarVideojuego(videojuego1);
                        Toast.makeText(getApplicationContext(), "Precio de salida actualizado", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog tituloP = alertaP.create();
        tituloP.setTitle("Añadir precio salida al videojuego");
        tituloP.show();
    }
    public void addPuntuacion(){
        EditText Puntuacion;
        Puntuacion = new EditText(MostrarJuegoActivity.this);
        Puntuacion.setInputType(InputType.TYPE_CLASS_NUMBER);
        AlertDialog.Builder alertaP = new AlertDialog.Builder(MostrarJuegoActivity.this);
        alertaP.setView(Puntuacion)
                .setMessage("Puntuar")
                .setCancelable(false)
                .setPositiveButton("Puntuar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                         Integer punt = Integer.parseInt(Puntuacion.getText().toString());
                            if(punt>=0 && punt<=10){
                                videojuego1.setMiPuntuacion(punt);
                                mViewModel.actualizarVideojuego(videojuego1);
                                Toast.makeText(getApplicationContext(), "Mi puntuación actualizado", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(getApplicationContext(), "La puntuación debe estar en un rango de 0-10", Toast.LENGTH_LONG).show();
                            }
                        }catch (NumberFormatException excepcion){
                            Toast.makeText(getApplicationContext(), "La puntuación debe estar en un rango de 0-10", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog tituloP = alertaP.create();
        tituloP.setTitle("Puntuar Videojuego");
        tituloP.show();
    }
    public void deleteVideojuego() {
        AlertDialog.Builder alerta = new AlertDialog.Builder(MostrarJuegoActivity.this);
        alerta.setMessage("¿Desea eliminar el videojuego?")
                .setCancelable(false)
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mViewModel.deleteVideojuego(videojuego1);
                        Intent intent = new Intent(MostrarJuegoActivity.this, MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Videojuego eliminado correctamente", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog titulo = alerta.create();
        titulo.setTitle("Eliminar Videojuego");
        titulo.show();

    }

    //Añade un juego a favororito, asi como lo elimina<
    public void Favorito(View view) {
        if (videojuego1.isEsFavorito() == false) {
            videojuego1.setEsFavorito(true); //Favorita
            //Actualizar como favorita
            mViewModel.actualizarVideojuego(videojuego1);
            EFbutton.setImageResource(R.drawable.ic_baseline_star_rate_24);
            Toast.makeText(getApplicationContext(), "Videojuego Añadido a Favoritos", Toast.LENGTH_LONG).show();
        } else {
            videojuego1.setEsFavorito(false); //Favorita
            //Cambiarlo por actualizar a no favorito
            mViewModel.actualizarVideojuego(videojuego1);
            EFbutton.setImageResource(R.drawable.ic_baseline_star_rate_25);
            Toast.makeText(getApplicationContext(), "Videojuego Eliminado de Favoritos", Toast.LENGTH_LONG).show();
        }
    }

}