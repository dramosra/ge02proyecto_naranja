package es.unex.giiis.asee.proyecto.gameover.ui;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.proyecto.gameover.data.Repository;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

/**
 * {@link ViewModel} for {@link MainActivity}
 */
class CrearJuegoViewModel extends ViewModel {

    private final Repository mRepository;

    public CrearJuegoViewModel(Repository repository) {
        mRepository = repository;

    }

    public void insertarVideojuego(Videojuego videojuego){
        mRepository.addVideojuego(videojuego);
    }


}
