package es.unex.giiis.asee.proyecto.gameover.ui;

import androidx.lifecycle.ViewModel;

import es.unex.giiis.asee.proyecto.gameover.data.Repository;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

/**
 * {@link ViewModel} for {@link MainActivity}
 */
class EditarVideojuegoViewModel extends ViewModel {

    private final Repository mRepository;

    public EditarVideojuegoViewModel(Repository repository) {
        mRepository = repository;
    }

    public void actualizarVideojuego(Videojuego videojuego){
        mRepository.updateVideojuego(videojuego);
    }
}
