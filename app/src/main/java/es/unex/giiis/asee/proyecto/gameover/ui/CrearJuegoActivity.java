package es.unex.giiis.asee.proyecto.gameover.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.proyecto.gameover.AppContainer;
import es.unex.giiis.asee.proyecto.gameover.AppExecutors;
import es.unex.giiis.asee.proyecto.gameover.MyApplication;
import es.unex.giiis.asee.proyecto.gameover.R;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

public class CrearJuegoActivity extends AppCompatActivity {

    private EditText titulo;
    private EditText valoracion;
    private EditText precio_Salida;
    private EditText precio_Venta;
    private EditText num_Ventas;
    private Button EFbutton;
    private AppExecutors mExecutors;
    Videojuego videojuego;
    private CrearJuegoViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setContentView(R.layout.activity_crear_juego);

        titulo = (EditText) findViewById(R.id.valorTitulo);
        valoracion = (EditText) findViewById(R.id.valorValoracion);
        precio_Salida = (EditText) findViewById(R.id.valorSalida);
        precio_Venta = (EditText) findViewById(R.id.valorRebajado);
        num_Ventas = (EditText) findViewById(R.id.valorVendidos);
        EFbutton = (Button) findViewById(R.id.button);
        mExecutors = AppExecutors.getInstance();

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.crearJuegoFactory).get(CrearJuegoViewModel.class);

        EFbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                titulo.setError(null);
                valoracion.setError(null);
                precio_Salida.setError(null);
                precio_Venta.setError(null);
                num_Ventas.setError(null);
                videojuego = new Videojuego();

                if("".equals(titulo.getText().toString())){
                    titulo.setError("Introduce un titulo");
                    titulo.requestFocus();
                    return;
                }else{
                    Log.i("Titulo",titulo.getText().toString());
                    videojuego.setTitle(titulo.getText().toString());
                }
                videojuego.setEsUsuario(true);
                videojuego.setEsFavorito(false);

                if("".equals(valoracion.getText().toString())){
                    valoracion.setError("Introduce la valoracion");
                    valoracion.requestFocus();
                    return;
                }else if(Integer.parseInt(valoracion.getText().toString())>=0&&Integer.parseInt(valoracion.getText().toString())<=10){
                    Log.i("Valoracion",valoracion.getText().toString());
                    videojuego.setDealRating(valoracion.getText().toString());
                }else {
                    valoracion.setError("La valoracion debe estar comprendida entre 0 y 10");
                    valoracion.requestFocus();
                    return;
                }

                if("".equals(precio_Salida.getText().toString())){
                    precio_Salida.setError("Introduce un precio de Salida");
                    precio_Salida.requestFocus();
                    return;
                }else if(precio_Venta.getText().toString().isEmpty()){
                    Log.i("precio_Salida",precio_Salida.getText().toString());
                    videojuego.setNormalPrice(precio_Salida.getText().toString());
                }else if(Integer.parseInt(precio_Salida.getText().toString())<Integer.parseInt(precio_Venta.getText().toString())){
                    precio_Salida.setError("El precio de salida es menor que el de rebaja");
                    precio_Salida.requestFocus();
                    return;
                }else{
                    Log.i("precio_Venta",precio_Salida.getText().toString());
                    videojuego.setNormalPrice(precio_Salida.getText().toString());
                }

                if("".equals(precio_Venta.getText().toString())){
                    precio_Venta.setError("Introduce un precio de Venta");
                    precio_Venta.requestFocus();
                    return;
                }else if(precio_Salida.getText().toString().isEmpty()) {
                    Log.i("precio_Venta",precio_Venta.getText().toString());
                    videojuego.setSalePrice(precio_Venta.getText().toString());
                }else if(Integer.parseInt(precio_Salida.getText().toString())<Integer.parseInt(precio_Venta.getText().toString())){
                    precio_Venta.setError("El precio de rebaja es mucho mayor que el de salida");
                    precio_Venta.requestFocus();
                    return;
                }else{
                    Log.i("precio_Venta",precio_Venta.getText().toString());
                    videojuego.setSalePrice(precio_Venta.getText().toString());
                }

                if("".equals(num_Ventas.getText().toString())){
                    num_Ventas.setError("Introduce un numero de ventas");
                    num_Ventas.requestFocus();
                    return;
                }else{
                    Log.i("num_Ventas",num_Ventas.getText().toString());
                    videojuego.setSavings(num_Ventas.getText().toString()+".00");
                }
                if (titulo.getError()==null&&valoracion.getError()==null&&precio_Salida.getError()==null&&precio_Venta.getError()==null&&num_Ventas.getError()==null) {
                    mViewModel.insertarVideojuego(videojuego);
                    Toast.makeText(getApplicationContext(), "Videojuego Creado", Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }

}