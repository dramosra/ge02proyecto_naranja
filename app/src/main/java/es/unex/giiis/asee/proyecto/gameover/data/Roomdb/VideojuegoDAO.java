package es.unex.giiis.asee.proyecto.gameover.data.Roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;


@Dao
public interface VideojuegoDAO {

    @Query("SELECT * FROM videojuegos WHERE id = :uid")
    public LiveData<Videojuego> getVideojuego(Integer uid);

    @Query("SELECT * FROM videojuegos WHERE usuarioVideojuego = 1 ORDER BY titulo ASC")
    public LiveData<List<Videojuego>> getVideojuegosCreados();

    @Query("SELECT * FROM videojuegos ORDER BY titulo ASC")
    public LiveData<List<Videojuego>> getAllVideojuegos();

    @Query("SELECT * FROM videojuegos WHERE videojuegoFavorito = 1 ORDER BY titulo ASC")
    public LiveData<List<Videojuego>> getFavs();

    @Query("DELETE FROM videojuegos WHERE usuarioVideojuego != 1")
    public void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertVideojuego(Videojuego item);

    @Delete
    public void deleteVideojuego(Videojuego item);

    @Update
    public void updateVideojuego(Videojuego item);


}
