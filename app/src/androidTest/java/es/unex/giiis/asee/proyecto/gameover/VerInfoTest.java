package es.unex.giiis.asee.proyecto.gameover;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.proyecto.gameover.ui.MainActivity;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class VerInfoTest {
    @Rule
    public ActivityTestRule<MainActivity> addPuntuacionTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void VerInfoTest() {
        //ACCEDER A UN ELEMENTO DEL RECYCLER VIEW - DE LA LISTA
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.recyclerId)).perform(RecyclerViewActions.actionOnItemAtPosition(0,click()));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}