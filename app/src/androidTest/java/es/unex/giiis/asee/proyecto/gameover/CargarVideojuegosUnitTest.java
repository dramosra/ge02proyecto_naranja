package es.unex.giiis.asee.proyecto.gameover;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import es.unex.giiis.asee.proyecto.gameover.data.Network.GameNetworkDataSource;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

@RunWith(MockitoJUnitRunner.class)
public class CargarVideojuegosUnitTest {

    @Mock
    GameNetworkDataSource listadoVideojuegos;

    // necessary to test the LiveData
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setup () {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getVideojuegos () throws ExecutionException, InterruptedException {
        List<Videojuego> videojuegos = new ArrayList<Videojuego>();
        Videojuego videojuego1 = createVideojuego(1);
        videojuegos.add(videojuego1);
        videojuegos.add(createVideojuego(2));
        MutableLiveData<List<Videojuego>> liveVideojuegos = new MutableLiveData<>();
        liveVideojuegos.postValue(videojuegos);
        when(listadoVideojuegos.getVideojuegosActuales()).thenReturn(liveVideojuegos);

        assertEquals(listadoVideojuegos.getVideojuegosActuales().getValue(),videojuegos);
        assertTrue(Objects.requireNonNull(listadoVideojuegos.getVideojuegosActuales().getValue()).size() == 2);
        assertTrue(listadoVideojuegos.getVideojuegosActuales().getValue().contains(videojuego1));
    }

    public static Videojuego createVideojuego(Integer id){
        Videojuego videojuego = new Videojuego();
        videojuego.setId(id);
        videojuego.setTitle("Superman");
        videojuego.setSavings("678");
        videojuego.setNormalPrice("89");
        videojuego.setSalePrice("14");
        videojuego.setDealRating("7");
        return videojuego;
    }
}
