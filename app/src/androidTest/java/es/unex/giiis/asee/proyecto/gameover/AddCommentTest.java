package es.unex.giiis.asee.proyecto.gameover;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static androidx.test.espresso.matcher.PreferenceMatchers.withTitle;
import static androidx.test.espresso.matcher.PreferenceMatchers.withTitleText;
import static androidx.test.espresso.matcher.ViewMatchers.isFocusable;
import static androidx.test.espresso.matcher.ViewMatchers.supportsInputMethods;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withInputType;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.instanceOf;

import android.view.MenuItem;
import android.widget.EditText;

import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Null;

import es.unex.giiis.asee.proyecto.gameover.R;
import es.unex.giiis.asee.proyecto.gameover.ui.MainActivity;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AddCommentTest {
    @Rule
    public ActivityTestRule<MainActivity> addPuntuacionTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void AddCommentTest() {
        String com = "Nuevo comentario";
        String titulo = "Nuevo";
        String precioR = "5";
        String precioS = "6";
        String valoracion = "6";
        String ventas = "5";
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_slideshow));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.fab)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.valorTitulo)).perform(typeText(titulo),closeSoftKeyboard());
        onView(withId(R.id.valorRebajado)).perform(typeText(precioR),closeSoftKeyboard());
        onView(withId(R.id.valorSalida)).perform(typeText(precioS),closeSoftKeyboard());
        onView(withId(R.id.valorValoracion)).perform(typeText(valoracion),closeSoftKeyboard());
        onView(withId(R.id.valorVendidos)).perform(typeText(ventas),closeSoftKeyboard());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.button)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.recyclerId)).perform(RecyclerViewActions.actionOnItemAtPosition(0,click()));
        openContextualActionModeOverflowMenu();
        onView(withText("Añadir Comentario")).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(supportsInputMethods()).perform(typeText(com),closeSoftKeyboard());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("COMENTAR")).perform(click());
    }
    @After
    public void deleteGame(){
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_slideshow));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.recyclerId)).perform(RecyclerViewActions.actionOnItemAtPosition(0,click()));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        openContextualActionModeOverflowMenu();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Eliminar juego")).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("SÍ")).perform(click());
    }
}