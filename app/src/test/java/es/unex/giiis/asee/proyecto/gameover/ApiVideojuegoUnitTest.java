package es.unex.giiis.asee.proyecto.gameover;

import static junit.framework.TestCase.assertTrue;

import static org.junit.Assert.assertFalse;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.proyecto.gameover.data.Network.GamesNetworkLoaderRunnable;
import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiVideojuegoUnitTest {
    @Test
    public void createApiVideojuegoTest() throws IOException {

        //we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("https://www.cheapshark.com/").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // we create a standard response to any request
        mockWebServer.enqueue(new MockResponse());
        //we link the mock server with our retrofit api

        GamesNetworkLoaderRunnable.gamesInterface service = retrofit.create(GamesNetworkLoaderRunnable.gamesInterface.class);

        // we create the call to add an appointment
        Call<List<Videojuego>> call = service.listGames();
        // we execute the call
        Response<List<Videojuego>> response = call.execute();

        //let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Finish web server
        mockWebServer.shutdown();
    }

    @Test
    public void getAllVideojuegosTest() throws IOException {

        //Step1: we create centers list
        Videojuego videojuego = new Videojuego();
        videojuego.setId(1);
        videojuego.setTitle("Videojuego 1");

        Videojuego videojuego1 = new Videojuego();
        videojuego1.setId(2);
        videojuego1.setTitle("Videojuego 2");

        List<Videojuego> videojuegos = new ArrayList<>();
        videojuegos.add(videojuego);
        videojuegos.add(videojuego1);

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(videojuegos));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        GamesNetworkLoaderRunnable.gamesInterface service = retrofit.create(GamesNetworkLoaderRunnable.gamesInterface.class);


        //Step6: we create the call to get centers
        Call<List<Videojuego>> call = service.listGames();
        // and we execute the call
        Response<List<Videojuego>> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Videojuego> videojuegosResponse =response.body();
        assertFalse(videojuegosResponse.isEmpty());
        assertTrue(videojuegosResponse.size()==2);

        //IMPORTANT: We need to have implemented "equals" in Center class to perform this assert
        assertTrue(videojuegosResponse.contains(videojuego));
        assertTrue(videojuegosResponse.contains(videojuego1));

        //Step9: Finish web server
        mockWebServer.shutdown();
    }

    @Test
    public void getListadoVideojuegosVacioTest() throws IOException {

        //Step1: we create empty centers list
        List<Videojuego> videojuegos = new ArrayList<>();


        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(videojuegos));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        GamesNetworkLoaderRunnable.gamesInterface service = retrofit.create(GamesNetworkLoaderRunnable.gamesInterface.class);



        //Step6: we create the call to get centers
        Call<List<Videojuego>> call = service.listGames();
        // and we execute the call
        Response<List<Videojuego>> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Videojuego> centersResponse =response.body();
        assertTrue(centersResponse.isEmpty());

        //Step9: Finish web server
        mockWebServer.shutdown();
    }



}