package es.unex.giiis.asee.proyecto.gameover;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import junit.framework.TestCase;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.asee.proyecto.gameover.data.model.Videojuego;

public class VideojuegoUnitTest {

    /*
    Pruebas Caso de uso: Añadir Videojuego
    */
    @Test
    public void setTitleCrearTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Cars";
        Videojuego instance = new Videojuego();
        instance.setTitle(value);
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setEsUsuarioCrearTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        Videojuego instance = new Videojuego();
        instance.setEsUsuario(value);
        final Field field = instance.getClass().getDeclaredField("esUsuario");
        field.setAccessible(true);
        TestCase.assertTrue(field.getBoolean(instance));
    }

    @Test
    public void setEsFavoritoCrearTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        Videojuego instance = new Videojuego();
        instance.setEsFavorito(value);
        final Field field = instance.getClass().getDeclaredField("esFavorito");
        field.setAccessible(true);
        TestCase.assertTrue(field.getBoolean(instance));
    }

    @Test
    public void setDealRatingCrearTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "7";
        Videojuego instance = new Videojuego();
        instance.setDealRating(value);
        final Field field = instance.getClass().getDeclaredField("dealRating");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setSalePriceCrearTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "45";
        Videojuego instance = new Videojuego();
        instance.setSalePrice(value);
        final Field field = instance.getClass().getDeclaredField("salePrice");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setNormalPriceCrearTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "4";
        Videojuego instance = new Videojuego();
        instance.setNormalPrice(value);
        final Field field = instance.getClass().getDeclaredField("normalPrice");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setSavingsCrearTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "800";
        Videojuego instance = new Videojuego();
        instance.setSavings(value);
        final Field field = instance.getClass().getDeclaredField("savings");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void equalsCrearTest(){
        Videojuego videojuego1 = new Videojuego();
        videojuego1.setId(1);
        videojuego1.setTitle("Juego1");

        Videojuego videojuego2 = new Videojuego();
        videojuego2.setId(2);
        videojuego2.setTitle("Juego2");

        Videojuego videojuego3 = new Videojuego();
        videojuego3.setId(1);
        videojuego3.setTitle("Juego3");

        assertEquals(videojuego1, videojuego1);
        assertNotEquals(videojuego2, videojuego3);
        assertEquals(videojuego1, videojuego3);

    }

    /*
    Pruebas Caso de uso: Puntuar Videojuego
    */
    @Test
    public void setMiPuntuacionPuntuarTest() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 5;
        Videojuego instance = new Videojuego();
        instance.setMiPuntuacion(value);
        final Field field = instance.getClass().getDeclaredField("miPuntuacion");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
    /*
     Pruebas Caso de uso: Añadir Comentario a Videojuego
    */
    @Test
    public void setComentarioNuevoComTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Me gusta este juego";
        Videojuego instance = new Videojuego();
        instance.setComentario(value);
        final Field field = instance.getClass().getDeclaredField("comentario");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
    /*
     Pruebas Caso de uso Añadir Videojuego a lista de favoritos
    */
    @Test
    public void setEsFavoritoAñadirTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        Videojuego instance = new Videojuego();
        instance.setEsFavorito(value);
        final Field field = instance.getClass().getDeclaredField("esFavorito");
        field.setAccessible(true);
        TestCase.assertTrue(field.getBoolean(instance));
    }

    @Test
    public void getIsEsFavoritoAñadirTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("esFavorito");
        field.setAccessible(true);
        field.set(instance,true);

        final Boolean result = instance.isEsFavorito();

        assertEquals("El campo no se ha recuperado correctamente", result, true);
    }

    /*
    Pruebas Caso de uso Borrar Comentario a Videojuego
    */
    @Test
    public void setComentarioBorrarComTest() throws NoSuchFieldException, IllegalAccessException {
        String value = " ";
        Videojuego instance = new Videojuego();
        instance.setComentario(value);
        final Field field = instance.getClass().getDeclaredField("comentario");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    /*
    Pruebas Caso de uso Editar Videojuego
    */
    @Test
    public void getTitleEditarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        field.set(instance, "FIFA 2021");

        final String result = instance.getTitle();

        assertEquals("El campo no se ha recuperado correctamente", result, "FIFA 2021");
    }

    @Test
    public void getDealRatingEditarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("dealRating");
        field.setAccessible(true);
        field.set(instance, "8");

        final String result = instance.getDealRating();

        assertEquals("El campo no se ha recuperado correctamente", result, "8");
    }

    @Test
    public void getNormalPriceEditarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("normalPrice");
        field.setAccessible(true);
        field.set(instance, "89");

        final String result = instance.getNormalPrice();

        assertEquals("El campo no se ha recuperado correctamente", result, "89");
    }

    @Test
    public void getSalePriceEditarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("salePrice");
        field.setAccessible(true);
        field.set(instance, "18");

        final String result = instance.getSalePrice();

        assertEquals("El campo no se ha recuperado correctamente", result, "18");
    }

    @Test
    public void getSavingsEditarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("savings");
        field.setAccessible(true);
        field.set(instance, "945");

        final String result = instance.getSavings();

        assertEquals("El campo no se ha recuperado correctamente", result, "945");
    }

    @Test
    public void getThumbEditarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("thumb");
        field.setAccessible(true);
        field.set(instance, "https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png");

        final String result = instance.getThumb();

        assertEquals("El campo no se ha recuperado correctamente", result, "https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png");
    }

    @Test
    public void setTitleEditarTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Batman";
        Videojuego instance = new Videojuego();
        instance.setTitle(value);
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setNormalPriceEditarTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "100";
        Videojuego instance = new Videojuego();
        instance.setNormalPrice(value);
        final Field field = instance.getClass().getDeclaredField("normalPrice");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setDealRatingEditarTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "4";
        Videojuego instance = new Videojuego();
        instance.setDealRating(value);
        final Field field = instance.getClass().getDeclaredField("dealRating");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
    @Test
    public void setSalePriceEditarTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "35";
        Videojuego instance = new Videojuego();
        instance.setSalePrice(value);
        final Field field = instance.getClass().getDeclaredField("salePrice");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setSavingsEditarTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "765";
        Videojuego instance = new Videojuego();
        instance.setSavings(value);
        final Field field = instance.getClass().getDeclaredField("savings");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    /*
   Pruebas Caso de uso Añadir Estado de Venta Videojuego
   */
    @Test
    public void setAnadirEstadoVentaATest() throws NoSuchFieldException, IllegalAccessException {
        String value = "SI";
        Videojuego instance = new Videojuego();
        instance.setEstadoVenta(value);
        final Field field = instance.getClass().getDeclaredField("estadoVenta");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
    /*
       Pruebas Caso de uso Añadir Precio de Salida a Videojuego
       */
    @Test
    public void setAnadirNormalPriceTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "900";
        Videojuego instance = new Videojuego();
        instance.setNormalPrice(value);
        final Field field = instance.getClass().getDeclaredField("normalPrice");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
    /*
    Pruebas Caso de uso Ver Informacion de Videojuego
    */
    @Test
    public void getTitleMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        field.set(instance, "Minecraft");

        final String result = instance.getTitle();

        assertEquals("El campo no se ha recuperado correctamente", result, "Minecraft");
    }

    @Test
    public void getDealRatingMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("dealRating");
        field.setAccessible(true);
        field.set(instance, "2");

        final String result = instance.getDealRating();

        assertEquals("El campo no se ha recuperado correctamente", result, "2");
    }

    @Test
    public void getNormalPriceMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("normalPrice");
        field.setAccessible(true);
        field.set(instance, "78");

        final String result = instance.getNormalPrice();

        assertEquals("El campo no se ha recuperado correctamente", result, "78");
    }

    @Test
    public void getSalePriceMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("salePrice");
        field.setAccessible(true);
        field.set(instance, "1");

        final String result = instance.getSalePrice();

        assertEquals("El campo no se ha recuperado correctamente", result, "1");
    }

    @Test
    public void getSavingsMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("savings");
        field.setAccessible(true);
        field.set(instance, "1200");

        final String result = instance.getSavings();

        assertEquals("El campo no se ha recuperado correctamente", result, "1200");
    }

    @Test
    public void getIsEsFavoritoMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("esFavorito");
        field.setAccessible(true);
        field.set(instance,true);

        final Boolean result = instance.isEsFavorito();

        assertEquals("El campo no se ha recuperado correctamente", result, true);
    }

    @Test
    public void getThumbMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("thumb");
        field.setAccessible(true);
        field.set(instance, "https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png");

        final String result = instance.getThumb();

        assertEquals("El campo no se ha recuperado correctamente", result, "https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png");
    }

    @Test
    public void setComentarioMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "Me gusta este juego";
        Videojuego instance = new Videojuego();
        instance.setComentario(value);
        final Field field = instance.getClass().getDeclaredField("comentario");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void setEsFavoritoMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        Videojuego instance = new Videojuego();
        instance.setEsFavorito(value);
        final Field field = instance.getClass().getDeclaredField("esFavorito");
        field.setAccessible(true);
        TestCase.assertTrue(field.getBoolean(instance));
    }

    @Test
    public void MostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("estadoVenta");
        field.setAccessible(true);
        field.set(instance, "SI");

        final String result = instance.getEstadoVenta();

        assertEquals("El campo no se ha recuperado correctamente", result, "SI");
    }

    @Test
    public void getComentarioMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("comentario");
        field.setAccessible(true);
        field.set(instance, "No me gusta este juego");

        final String result = instance.getComentario();

        assertEquals("El campo no se ha recuperado correctamente", result, "No me gusta este juego");
    }

    @Test
    public void getMiPuntuacionMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("miPuntuacion");
        field.setAccessible(true);
        field.set(instance, new Integer(10));

        final Integer result = instance.getMiPuntuacion();

        assertEquals("El campo no se ha recuperado correctamente", result, new Integer(10));
    }

    @Test
    public void getIsEsUsuarioMostrarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("esUsuario");
        field.setAccessible(true);
        field.set(instance,true);

        final Boolean result = instance.isEsUsuario();

        assertEquals("El campo no se ha recuperado correctamente", result, true);
    }

    /*
    Pruebas Caso de uso Añadir Estado de Venta Videojuego
    */
    @Test
    public void getTitleMostrarListadoTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        field.set(instance, "ET");

        final String result = instance.getTitle();

        assertEquals("El campo no se ha recuperado correctamente", result, "ET");
    }

    @Test
    public void getSalePriceMostrarListadoTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("salePrice");
        field.setAccessible(true);
        field.set(instance, "10");

        final String result = instance.getSalePrice();

        assertEquals("El campo no se ha recuperado correctamente", result, "10");
    }

    @Test
    public void getThumbMostrarListadoTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("thumb");
        field.setAccessible(true);
        field.set(instance, "https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png");

        final String result = instance.getThumb();

        assertEquals("El campo no se ha recuperado correctamente", result, "https://http2.mlstatic.com/storage/mshops-appearance-api/images/15/254304515/logo-2020060212005277900.png");
    }

    @Test
    public void getIsEsUsuarioMostrarListadoTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("esUsuario");
        field.setAccessible(true);
        field.set(instance,true);

        final Boolean result = instance.isEsUsuario();

        assertEquals("El campo no se ha recuperado correctamente", result, true);
    }

    @Test
    public void getSavingsListadoTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("savings");
        field.setAccessible(true);
        field.set(instance, "54");

        final String result = instance.getSavings();

        assertEquals("El campo no se ha recuperado correctamente", result, "54");
    }

    /*
    Pruebas Caso de uso Buscar Videojuego
    */
    @Test
    public void getTitleBuscarTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        field.set(instance, "Borderlands");

        final String result = instance.getTitle();

        assertEquals("El campo no se ha recuperado correctamente", result, "Borderlands");
    }

    /*
    Pruebas Caso de uso Eliminar Puntuacion a Videojuego
    */
    @Test
    public void setMiPuntuacionEliminarTest() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 0;
        Videojuego instance = new Videojuego();
        instance.setMiPuntuacion(value);
        final Field field = instance.getClass().getDeclaredField("miPuntuacion");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    /*
    Pruebas Caso de uso Videojuegos mas Vendidos
    */
    @Test
    public void getMasSavingsTest() throws NoSuchFieldException, IllegalAccessException {
        final Videojuego instance = new Videojuego();
        final Field field = instance.getClass().getDeclaredField("savings");
        field.setAccessible(true);
        field.set(instance, "45");

        final String result = instance.getSavings();

        assertEquals("El campo no se ha recuperado correctamente", result, "45");
    }
}